defmodule SimpleMath do

  def perform() do

    import IOUtils

    n1 = read_int("What is the first number? ")
    n2 = read_int("What is the second number? ")

    IO.puts "#{n1} + #{n2} = #{n1 + n2}"
    IO.puts "#{n1} - #{n2} = #{n1 - n2}"
    IO.puts "#{n1} * #{n2} = #{n1 * n2}"
    IO.puts "#{n1} / #{n2} = #{n1 / n2}"

  end

end
