defmodule Counter do

  def count() do
    import IOUtils
    data = read_trimmed_string("What is the input string? ")
    no_of_chars = String.length(data)
    IO.puts "#{data} has #{no_of_chars} characters"
  end

end
