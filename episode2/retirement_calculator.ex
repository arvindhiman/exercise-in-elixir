defmodule RetirementCalculator do

  import IOUtils

  def execute() do
    {age, retirement_age} = _get_user_inputs()
    years_left = calculate_years_left(age, retirement_age)
    current_year = DateTime.utc_now().year
    year_of_retirement = calculate_year_of_retirement(current_year, years_left)
    _print_output(years_left, current_year, year_of_retirement)
  end

  defp _get_user_inputs() do
    age = read_int("What is your age? ")
    retirement_age = read_int("At what age will you like to retire? ")
    {age, retirement_age}
  end

  defp _print_output(years_left, current_year, year_of_retirement) when years_left > 0 do
    IO.puts("You have #{years_left} years left until you can retire")
    IO.puts("It's #{current_year}, so you can retire in #{year_of_retirement}.")
  end

  defp _print_output(years_left, _, _) when years_left < 0 do
    IO.puts("Looks like you have already retired. Enjoy your retirement!!")
  end

  defp _print_output(years_left, _, _) when years_left == 0 do
    IO.puts("Either you have retired or going to retire this year!!!")
  end

  def calculate_years_left(age, retirement_age) do
      retirement_age - age
  end

  def calculate_year_of_retirement(from_year, no_of_years_left) do
    from_year + no_of_years_left
  end

end
