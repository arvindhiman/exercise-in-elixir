defmodule MadLib do

  def play() do
    import IOUtils
    noun = read_trimmed_string("Enter a noun: ")
    verb = read_trimmed_string("Enter a verb: ")
    adjective = read_trimmed_string("Enter a adjective: ")
    adverb = read_trimmed_string("Enter a adverb: ")

    IO.puts "Do you #{verb} your #{adjective} #{noun} #{adverb}? That's hilarious!"
  end



end
