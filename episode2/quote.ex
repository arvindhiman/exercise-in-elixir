defmodule Quote do

  def ask_and_print() do
    import IOUtils
    quote = read_trimmed_string("What is the quote? ")
    quotee = read_trimmed_string("Who said it? ")
    IO.puts "#{quotee} says, \"#{quote}\""
  end

end
