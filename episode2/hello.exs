defmodule Hello do

  def greet() do
    import IOUtils
    name = read_trimmed_string("What is your name? ")
    IO.puts "Hello, #{name}, nice to meet you!"
  end

end
