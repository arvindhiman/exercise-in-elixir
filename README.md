# exercise-in-elixir
Learning Elixir by following [Exercises for Programmer](https://pragprog.com/book/bhwb/exercises-for-programmers) book

## Elixir and Erlang versions

Erlang/OTP 20 [erts-9.1]

Elixir 1.5.3

## Chapter 2
To run chapter 2 code, use iex.

For example to run "counter.ex" using iex; first load io_utils.ex which has reusable functions.

```
$ iex io_utils.ex
iex(1)> c "counter.ex"
[Counter]
iex(2)> Counter.count
```
