defmodule PaintCalculator do
  @moduledoc """
   unit 7 - Paint Calculator
  """

  @square_feet_covered_by_one_gallon 350

  @doc """
   calculate gallons of paint needed for area in square feet.
   rounds to nearest whole number

  ## Examples

      iex> PaintCalculator.calculate(360)
      2.0

  """
  def calculate(square_feet_area) do
    Float.ceil(square_feet_area / @square_feet_covered_by_one_gallon)
  end


end
