defmodule IOUtils do

  def read_int(prompt) do
    data = IO.gets(prompt)
    data = String.trim(data, "\n")
    String.to_integer(data)
  end

  def read_trimmed_string(prompt) do
    String.trim(IO.gets(prompt), "\n")
  end

end
