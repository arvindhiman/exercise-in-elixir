defmodule RectangleAreaCalculator do
  @moduledoc """
   unit 7 - Area of a rectangular room
  """
  @feet_to_meter_conversion_metric 0.09290304

  @doc """
   calculate area for a give rectangle length and width

  ## Examples

      iex> RectangleAreaCalculator.calculate(10, 18)
      180

  """
  def calculate(length, width) do
    length * width
  end

  @doc """
   converts feet into meters

  ## Examples

      iex> RectangleAreaCalculator.convert_feet_to_meters(300)
      27.870912

  """
  def convert_feet_to_meters(feet) do
    feet * @feet_to_meter_conversion_metric
  end

  @doc """
   support command line user input of length and width,
   calculates area and prints area in square feet and square meters
  """
  def cli_calculate() do
    import IOUtils

    length = read_int("What is the length of the room in feet? ")
    width = read_int("What is the width of the room in feet? ")
    IO.puts "You entered dimensions of #{length} feet by #{width} feet."
    area_in_feet = calculate(length, width)
    area_in_meters = convert_feet_to_meters(area_in_feet)
    IO.puts "The area is"
    IO.puts "#{area_in_feet} square feet"
    IO.puts "#{area_in_meters} square meter"

  end
end
