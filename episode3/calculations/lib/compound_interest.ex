defmodule CompoundInterest do
  @moduledoc """
   unit 7 - Compound Interest
  """

  @doc """
   calculate compound interest

  ## Examples

      iex> CompoundInterest.calculate(1500, 4.3, 6, 4)
      1938.84

  """
  def calculate(principal, rate_percent, no_of_years, compounding_frequency) do
    base = 1 + (rate_percent/100)/compounding_frequency
    power = compounding_frequency * no_of_years
    base_to_power = pow(base, power)
    principal * base_to_power
      |> Float.round(2)
  end

  def  pow(n, k), do: pow(n, k, 1)
  defp pow(_, 0, acc), do: acc
  defp pow(n, k, acc), do: pow(n, k - 1, n * acc)

end
