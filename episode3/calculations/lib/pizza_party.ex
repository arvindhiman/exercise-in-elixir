defmodule PizzaParty do
  @moduledoc """
   unit 7 - Pizza Party
  """

  @slices_in_a_pizza 8

  @doc """
   divide pizza among persons, assumes each pizza has 8 slices

  ## Examples

      iex> PizzaParty.equally_divide_pizzas(2, 8)
      {2, 0}

  """
  def equally_divide_pizzas(no_of_pizza, no_of_persons) do
    total_slices = no_of_pizza * @slices_in_a_pizza
    no_of_slices_per_person = div(total_slices, no_of_persons)
    slices_left = rem(total_slices, no_of_persons)
    {no_of_slices_per_person, slices_left}
  end

  @doc """
   support command line user input of pizzas and people,
  """
  def cli_divide() do
    import IOUtils

    people = read_int("How many people? ")
    pizzas = read_int("How many pizzas? ")
    {no_of_slices_per_person, slices_left}
        = equally_divide_pizzas(pizzas, people)

    IO.puts """
      #{people} people with #{pizzas} pizzas
      Each person get #{no_of_slices_per_person} slices of pizza.
      There are #{slices_left} leftover slices.
    """
  end
end
