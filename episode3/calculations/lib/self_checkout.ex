defmodule SelfCheckout do
  @moduledoc """
   unit 7 - Self Checkout
  """
  @tax_rate 0.08

  @doc """
   calculate total with tax for given set of items
   with quantity and price

  ## Examples

      iex> SelfCheckout.calculate([{"item1", 25, 2}, {"item2", 10, 1}])
      64.80

  """
  def calculate(cart) do
    sum = subtotal(cart, 0)
    tax = calculate_tax(sum)
    sum + tax
  end

  def calculate_tax(subtotal_amount) do
    @tax_rate * subtotal_amount
  end

  defp subtotal([{_, price, quantity}| tail], sum) do
    sum = sum + price * quantity
    subtotal(tail, sum)
  end

  defp subtotal([], sum) do
    sum
  end

end
