defmodule PaintCalculatorTest do
  use ExUnit.Case
  doctest PaintCalculator

  test "requires 2 gallons for 360 sq. ft." do
    assert PaintCalculator.calculate(360) == 2.0
  end

end
