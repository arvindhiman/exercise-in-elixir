defmodule PizzaPartyTest do
  use ExUnit.Case
  doctest PizzaParty

  test "equally divide 2 pizzas among 8 persons" do
    assert PizzaParty.equally_divide_pizzas(2, 8) == {2, 0}
  end

  test "equally divide 2 pizzas among 9 persons" do
    assert PizzaParty.equally_divide_pizzas(2, 9) == {1, 7}
  end

end
