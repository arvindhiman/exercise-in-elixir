defmodule SelfCheckoutTest do
  use ExUnit.Case
  doctest SelfCheckout

  test "cart with 3 items total" do
    cart = [
      {"item1", 25, 2},
      {"item2", 10, 1}
    ]
    assert SelfCheckout.calculate(cart) == 64.8
  end

end
