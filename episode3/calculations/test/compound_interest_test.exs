defmodule CompoundInterestTest do
  use ExUnit.Case
  doctest CompoundInterest

  test "calculate compound interest" do
    assert CompoundInterest.calculate(1500, 4.3, 6, 4) == 1938.84
  end

end
