defmodule RectangleAreaCalculatorTest do
  use ExUnit.Case
  doctest RectangleAreaCalculator

  test "calculate area" do
    assert RectangleAreaCalculator.calculate(10, 18) == 180
  end

  test "convert feet to meters" do
    assert RectangleAreaCalculator.convert_feet_to_meters(300) == 27.870912 
  end
end
